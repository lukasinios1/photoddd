package com.example.makimo.entities

data class ImageViewEntity(
    val imageName: String = "",
    val imageCreateDate: String = ""
)