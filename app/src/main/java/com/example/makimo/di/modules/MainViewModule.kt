package com.example.makimo.di.modules

import androidx.lifecycle.ViewModel
import com.example.makimo.ui.MainActivity
import com.example.makimo.ui.MainViewModel
import com.example.makimo.utils.annotations.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(
    includes = [
        MainModule::class]
)
abstract class MainViewModule {
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel
}