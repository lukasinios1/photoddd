package com.example.makimo.di.modules

import android.app.Application
import com.example.makimo.App
import dagger.Binds
import dagger.Module

@Module
internal abstract class AppModule {

    @Binds
    abstract fun bindApplication(application: App): Application
}
