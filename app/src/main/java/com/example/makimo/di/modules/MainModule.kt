package com.example.makimo.di.modules

import android.app.Application
import com.example.data.repositories.ImageRepositoryImpl
import com.example.data.repositories.FileRepositoryImpl
import com.example.domain.repositories.ImageRepository
import com.example.domain.repositories.FileRepository
import com.example.domain.usecase.ImagesUseCase
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class MainModule {

    @Provides
    @Reusable
    fun provideImageRepository(): ImageRepository = ImageRepositoryImpl()

    @Provides
    @Reusable
    fun provideGetImagePathUseCase(application: Application): FileRepository =
        FileRepositoryImpl(application)

    @Provides
    @Reusable
    fun provideSaveImageUseCase(
        imageRepository: ImageRepository,
        localStorageRepositoryImpl: FileRepositoryImpl
    ): ImagesUseCase = ImagesUseCase(imageRepository, localStorageRepositoryImpl)
}