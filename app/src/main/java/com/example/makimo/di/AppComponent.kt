package com.example.makimo.di

import com.example.makimo.App
import com.example.makimo.di.modules.AppModule
import com.example.makimo.di.modules.MainViewModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        MainViewModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Factory<App> {

        override fun create(application: App): AppComponent {
            seedApplication(application)
            return build()
        }

        @BindsInstance
        internal abstract fun seedApplication(application: App)

        internal abstract fun build(): AppComponent

    }
}