package com.example.makimo.di.factory

import androidx.lifecycle.ViewModelProvider
import com.example.makimo.di.modules.AppModule
import dagger.Binds
import dagger.Module

@Module(includes = [AppModule::class])
abstract class ViewModelFactoryModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}

