package com.example.makimo.ui

import androidx.recyclerview.widget.DiffUtil
import com.example.makimo.entities.ImageViewEntity

class ImageDiffUtil(
    private val oldImages: List<ImageViewEntity>,
    private val newImages: List<ImageViewEntity>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldImages[oldItemPosition] == newImages[newItemPosition]

    override fun getOldListSize(): Int = oldImages.size

    override fun getNewListSize(): Int = newImages.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldImages[oldItemPosition].imageCreateDate == newImages[newItemPosition].imageCreateDate &&
                oldImages[oldItemPosition].imageName == newImages[newItemPosition].imageName

}