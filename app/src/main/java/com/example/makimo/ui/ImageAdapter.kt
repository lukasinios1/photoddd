package com.example.makimo.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.makimo.R
import com.example.makimo.entities.ImageViewEntity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.image_item.view.*
import java.io.File


class ImageAdapter(private val getImageByName: (name: String) -> File?) :
    RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {

    var imageEntityList = mutableListOf<ImageViewEntity>()
        set(value) {
            DiffUtil.calculateDiff(
                ImageDiffUtil(
                    imageEntityList,
                    value
                )
            ).also { diffUtilResult ->
                imageEntityList.clear()
                imageEntityList.addAll(value)
                diffUtilResult.dispatchUpdatesTo(this)
            }
        }

    fun addSingleEntity(imageViewEntity: ImageViewEntity) {
        val newList = mutableListOf<ImageViewEntity>()
        newList.addAll(imageEntityList)
        newList.add(imageViewEntity)

        DiffUtil.calculateDiff(
            ImageDiffUtil(
                imageEntityList,
                newList
            )
        ).also { diffUtilResult ->
            imageEntityList.clear()
            imageEntityList.addAll(newList)
            diffUtilResult.dispatchUpdatesTo(this)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.image_item, parent, false)
        return ImageViewHolder(view, getImageByName)
    }

    override fun getItemCount(): Int = imageEntityList.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(imageEntityList[position])
    }

    class ImageViewHolder(
        private val view: View,
        private val getImageByName: (name: String) -> File?
    ) : RecyclerView.ViewHolder(view) {
        fun bind(imageViewEntity: ImageViewEntity) {
            view.image_title.text = imageViewEntity.imageName
            view.image_create_date.text = imageViewEntity.imageCreateDate

            getImageByName(imageViewEntity.imageName)?.let { notNullImage ->
                Picasso.get()
                    .load(notNullImage)
                    .fit()
                    .centerCrop()
                    .into(view.image_view)
            }

        }
    }
}