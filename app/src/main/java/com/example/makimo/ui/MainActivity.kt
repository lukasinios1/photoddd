package com.example.makimo.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.makimo.R
import com.example.makimo.di.factory.ViewModelFactory
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var factory: ViewModelFactory

    private val mainViewmodel: MainViewModel by lazy {
        ViewModelProvider(this, factory).get(MainViewModel::class.java)
    }

    private val imageAdapter: ImageAdapter by lazy {
        ImageAdapter(mainViewmodel::getImageByName)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recycler_view.adapter = imageAdapter
        mainViewmodel.getImagesHeader()
        take_photo_button.setOnClickListener {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), MY_CAMERA_PERMISSION_CODE)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            }
        }

        mainViewmodel.mainActionLiveData.observe(this, Observer { mainAction ->
            when (mainAction) {
                is MainViewModel.MainAction.Error -> {
                    Toast.makeText(this, mainAction.message, Toast.LENGTH_LONG).show()
                }
                is MainViewModel.MainAction.AddSingleImageHeader -> {
                    imageAdapter.addSingleEntity(mainAction.imageViewEntity)
                }
                is MainViewModel.MainAction.ImageHeadersFetched -> {
                    imageAdapter.imageEntityList = mainAction.imageViewEntities.toMutableList()
                }
            }
        })


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            } else {
                Toast.makeText(this, getString(R.string.denied_permission), Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            val photo = data?.extras?.get(DATA) as Bitmap?
            photo?.let { notNullPhoto ->
                mainViewmodel.saveImage(notNullPhoto)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        private const val CAMERA_REQUEST = 1887
        private const val MY_CAMERA_PERMISSION_CODE = 100
        private const val DATA = "data"
    }
}
