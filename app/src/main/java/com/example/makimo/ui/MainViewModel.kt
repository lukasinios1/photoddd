package com.example.makimo.ui

import android.graphics.Bitmap
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.usecase.ImagesUseCase
import com.example.makimo.entities.ImageViewEntity
import kotlinx.coroutines.*
import java.io.File
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class MainViewModel @Inject constructor(private val imagesUseCase: ImagesUseCase) : ViewModel() {

    val mainActionLiveData: MutableLiveData<MainAction> = MutableLiveData()


    fun getImageByName(name: String): File? {
        var file: File? = null
        runBlocking {
            file = imagesUseCase.getImageByName(name).image
        }
        return file
    }

    fun saveImage(image: Bitmap) {
        GlobalScope.launch(Dispatchers.Main) {
            val saveImage = imagesUseCase.saveImage(image, "")
            if (saveImage.isStatusOk) {
                val strDate: String = getStringDate(saveImage.createDate)
                mainActionLiveData.value =
                    MainAction.AddSingleImageHeader(ImageViewEntity(saveImage.imageName, strDate))
            } else {
                mainActionLiveData.value = MainAction.Error(saveImage.errorMessage.orEmpty())
            }
        }
    }

    private fun getStringDate(date: Date): String {
        val dateFormat: DateFormat = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())
        return dateFormat.format(date)
    }

    fun getImagesHeader() {
        GlobalScope.launch(Dispatchers.Main) {
            val imageHeaders = imagesUseCase.getAllImagesHeaders().awaitAll().map { imageHeader ->
                ImageViewEntity(
                    imageHeader.imageName,
                    getStringDate(imageHeader.createDate)
                )
            }
            mainActionLiveData.value = MainAction.ImageHeadersFetched(imageHeaders)
        }
    }


    sealed class MainAction {
        data class Error(val message: String) : MainAction()
        data class ImageHeadersFetched(val imageViewEntities: List<ImageViewEntity>) : MainAction()
        data class AddSingleImageHeader(val imageViewEntity: ImageViewEntity) : MainAction()
    }

    companion object{
        const val DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
    }
}