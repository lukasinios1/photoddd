package com.example.domain.usecase

import android.graphics.Bitmap
import com.example.domain.entities.DomainImageHeader
import com.example.domain.entities.DomainImageResponse
import com.example.domain.repositories.ImageRepository
import com.example.domain.repositories.FileRepository
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.io.File
import kotlin.random.Random

class ImagesUseCase(
    private val imageRepository: ImageRepository,
    private val fileRepository: FileRepository
) {

    suspend fun getImageByName(imageName: String): DomainImageResponse {
        val myDir = fileRepository.getFileDirectory(IMAGE_CATALOG_NAME)
        return imageRepository.getImage(myDir, imageName)
    }

    suspend fun getAllImagesHeaders(): List<Deferred<DomainImageHeader>> {
        val imageHeaderTaskList: MutableList<Deferred<DomainImageHeader>> = mutableListOf()
        fileRepository.getAllFileNamesFromDirectory(fileRepository.getCatalogPath(IMAGE_CATALOG_NAME))
            .forEach { fileName ->
                imageHeaderTaskList.add(GlobalScope.async {
                    val myDir = fileRepository.getFileDirectory(IMAGE_CATALOG_NAME)
                    imageRepository.getImageHeader(myDir, fileName)
                })
            }
        return imageHeaderTaskList
    }

    suspend fun saveImage(image: Bitmap, imageName: String): DomainImageHeader {
        val fileName = if (imageName.isBlank()) Random.nextLong(
            Long.MIN_VALUE,
            Long.MAX_VALUE
        ).toString() else imageName

        val myDir = fileRepository.getFileDirectory(IMAGE_CATALOG_NAME)

        if (!myDir.exists()) {
            myDir.mkdirs()
        }

        val file = File(myDir, fileName)
        if (file.exists()) file.delete()
        val saveStatus = imageRepository.saveImage(file, image, DEFAULT_JPG_QUANTITY)
        return if (saveStatus.isStatusOk) {
            imageRepository.getImageHeader(
                myDir,
                fileName
            )
        } else {
            DomainImageHeader().apply {
                this.isStatusOk = saveStatus.isStatusOk
                this.errorMessage = saveStatus.errorMessage
            }
        }
    }


    companion object {
        const val DEFAULT_JPG_QUANTITY = 100
        const val IMAGE_CATALOG_NAME = "saved_images"
    }
}