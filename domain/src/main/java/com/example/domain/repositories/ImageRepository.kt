package com.example.domain.repositories

import android.graphics.Bitmap
import com.example.domain.entities.DomainImageResponse
import com.example.domain.entities.DomainImageStatus
import com.example.domain.entities.DomainImageHeader
import java.io.File

interface ImageRepository {

    suspend fun saveImage(imageDirectory: File, image: Bitmap, quantity: Int): DomainImageStatus

    suspend fun getImage(imageDirectory: File, fileName: String): DomainImageResponse

    suspend fun getImageHeader(imageDirectory: File, fileName: String): DomainImageHeader
}