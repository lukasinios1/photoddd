package com.example.domain.repositories

import java.io.File

interface FileRepository {

    fun getAllFileNamesFromDirectory(path: String): List<String>

    fun getFileDirectory(catalogName: String): File

    fun getCatalogPath(catalogName: String): String
}