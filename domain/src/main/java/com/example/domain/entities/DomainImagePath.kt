package com.example.domain.entities

import java.io.File

data class DomainImagePath(
    val fileName: String,
    val catalogName: String,
    val fileDir: File
)