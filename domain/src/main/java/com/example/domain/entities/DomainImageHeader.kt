package com.example.domain.entities

import java.util.*

data class DomainImageHeader(
    val imageName: String = "",
    val createDate: Date = Calendar.getInstance().time
) : DomainImageStatus()