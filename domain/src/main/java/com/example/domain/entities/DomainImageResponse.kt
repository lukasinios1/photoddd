package com.example.domain.entities

import java.io.File
import java.time.Instant
import java.util.*

data class DomainImageResponse(
    val image: File? = null
) : DomainImageStatus()