package com.example.domain.entities

import android.graphics.Bitmap

data class DomainImageToSave(
    val image: Bitmap,
    val imageQuality: Int,
    val domainImagePath: DomainImagePath
)