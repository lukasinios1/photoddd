package com.example.domain.entities

open class DomainImageStatus(
    var isStatusOk: Boolean = true,
    var errorMessage: String? = null
)