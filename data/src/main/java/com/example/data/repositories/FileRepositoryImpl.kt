package com.example.data.repositories

import android.app.Application
import com.example.domain.repositories.FileRepository
import java.io.File
import javax.inject.Inject

class FileRepositoryImpl @Inject constructor(private val application: Application) :
    FileRepository {

    override fun getAllFileNamesFromDirectory(path: String): List<String> {
        val directory = File(path)
        val files = directory.listFiles()
        return files?.map { file -> file.name }.orEmpty()
    }

    override fun getFileDirectory(catalogName: String): File = File(
        application.applicationContext.filesDir,
        catalogName
    )

    override fun getCatalogPath(catalogName: String): String =
        "${application.applicationContext.filesDir.absolutePath}/${catalogName}/"
}