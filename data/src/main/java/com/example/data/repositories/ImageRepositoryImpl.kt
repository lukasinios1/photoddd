package com.example.data.repositories

import android.app.Application
import android.graphics.Bitmap
import com.example.domain.entities.DomainImageHeader
import com.example.domain.entities.DomainImageResponse
import com.example.domain.entities.DomainImageStatus
import com.example.domain.repositories.ImageRepository
import com.example.domain.usecase.ImagesUseCase.Companion.IMAGE_CATALOG_NAME
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.util.*
import javax.inject.Inject

class ImageRepositoryImpl: ImageRepository {

    override suspend fun saveImage(
        imageDirectory: File,
        image: Bitmap,
        quantity: Int
    ): DomainImageStatus {
        require(image.byteCount > 0) {
            "image size must by at least 1 but was: ${image.byteCount}"
        }
        require(quantity > 0) {
            "image quantity must by at least 1 but was: $quantity"
        }

        return withContext(Dispatchers.IO) {
            try {
                val out = FileOutputStream(imageDirectory)
                image.compress(
                    Bitmap.CompressFormat.JPEG,
                    quantity,
                    out
                )

                out.flush()
                out.close()
                return@withContext DomainImageStatus()
            } catch (exception: Exception) {
                exception.printStackTrace()
                return@withContext DomainImageStatus(
                    isStatusOk = false,
                    errorMessage = exception.message
                )
            }
        }
    }

    override suspend fun getImage(imageDirectory: File, fileName: String): DomainImageResponse {
        require(fileName.isNotBlank()) {
            "image name  is blank!"
        }
        require(imageDirectory.isDirectory) {
            "file is not directory!"
        }
        try {
            return if (imageDirectory.exists()) {
                val file = File(imageDirectory, fileName)
                if (file.exists()) return DomainImageResponse(
                    file
                ) else DomainImageResponse().apply {
                    this.errorMessage = "file ${file.absolutePath} don't exits"
                    this.isStatusOk = false
                }
            } else {
                DomainImageResponse().apply {
                    this.errorMessage = "directory ${imageDirectory.absolutePath} don't exits"
                    this.isStatusOk = false
                }
            }
        } catch (exception: Exception) {
            return DomainImageResponse().apply {
                this.errorMessage = exception.message
                this.isStatusOk = false
            }
        }
    }

    override suspend fun getImageHeader(imageDirectory: File, fileName: String): DomainImageHeader {
        require(fileName.isNotBlank()) {
            "image name  is blank!"
        }
        require(imageDirectory.isDirectory) {
            "file is not directory!"
        }
        try {
            return if (imageDirectory.exists()) {
                val file = File(imageDirectory, fileName)
                if (file.exists()) return DomainImageHeader(
                    file.name,
                    Date(file.lastModified())
                ) else DomainImageHeader().apply {
                    this.errorMessage = "file ${file.absolutePath} don't exits"
                    this.isStatusOk = false
                }
            } else {
                DomainImageHeader().apply {
                    this.errorMessage = "directory ${imageDirectory.absolutePath} don't exits"
                    this.isStatusOk = false
                }
            }
        } catch (exception: Exception) {
            return DomainImageHeader().apply {
                this.errorMessage = exception.message
                this.isStatusOk = false
            }
        }
    }
}

